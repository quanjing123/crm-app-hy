import store from '../store/index'

var Push = function(test) {}
/**  
 *  
 * 推送的监听  
 */
Push.prototype.pushListener = function() {
	var _this = this;
	plus.push.addEventListener("click", function(msg) {
		console.log(msg);
		let path = "";
		try {
			plus.push.clear();
			plus.runtime.setBadgeNumber(0);
			if (plus.os.name == 'iOS') {
				var GeTuiSdk = plus.ios.importClass('GeTuiSdk');
				GeTuiSdk.setBadge(0);
			}
			if (msg.payload) {
				path = msg.payload;
				if (msg.aps && msg.payload.payload) {
					path = msg.payload.payload;
				}
			}
			//
			if (path) {
				if (path.indexOf('http') > -1) {
					uni.setStorageSync('_H5URL_', path)
					uni.navigateTo({
						url: 'pages/jxs/h5Temp'
					});
					return;
				}
				uni.navigateTo({
					url: path
				});
			}
		} catch (e) {
			console.error(e);
		}
	}, false);

	plus.push.addEventListener("receive", function(msg) {
		console.log(msg);
		if (msg.aps) { // Apple APNS message  
			console.log("接收到在线APNS消息：");
		} else {
			console.log("接收到在线透传消息：");
			store.commit('NEW_MSG', msg);
			if (plus.os.name == 'iOS') {
				if (msg.payload) {
					try {
						if (msg.title != '红木名片') {
							// _this.notificationMessage(msg);
						}
					} catch (e) {
						console.log(e);
					}
				}
			}
		}
	}, false);
}

/**  
 * 根据推送消息在通知栏中显现对应的提示  
 * @param {Object} msg  
 */
Push.prototype.notificationMessage = function(msg) {
	var content = msg.content; //你要展示的提示  
	var jsonData = '';
	switch (plus.os.name) {
		case "Android":
			jsonData = eval("(" + msg.payload + ")");
			break;
		case "iOS":
			jsonData = msg.payload;
			break;
	}
	plus.push.createMessage(msg.title, msg.payload, {});
}

/**  
 * 处理通知方法  
 * @param {Object} msg  
 */
Push.prototype.handle = function(msg) {
	plus.push.clear();
}
Push.prototype.cancelPushClear = function() {}

module.exports.Push = Push
