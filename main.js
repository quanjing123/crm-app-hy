import Vue from 'vue'
import App from './App'
import './static/css/style.css'
import './static/css/icon-font.css';
import store from './store/index'
import mixins from './mixins';

Vue.config.productionTip = false;
Vue.prototype.$store = store;
App.mpType = 'app'

Vue.mixin(mixins);

const app = new Vue(App)
app.$mount()







































