import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)
const state = {
	currentCompany: {},
	currentClient: {},
	newMsg: 0 // 0初始状态1消息2其他
};

const mutations = {
	SET_CURRENT_COMPANY(state, data) {
		state.currentCompany = Object.assign({}, state.currentCompany, data);
	},
	SET_CURRENT_CLIENT(state, data) {
		state.currentClient = data;
	},
	NEW_MSG(state, data) {
		state.newMsg = data;
	}
};

const actions = {
	setCurrentCompany({
		commit
	}, data) {
		commit('SET_CURRENT_COMPANY', data)
	},
	setCurrentClient({
		commit
	}, data) {
		commit('SET_CURRENT_CLIENT', data)
	},
};

export default new Vuex.Store({
	state,
	mutations,
	actions,
	modules: {},
	getters: {
		currentCompany: state => state.currentCompany,
		currentClient: state => state.currentClient,
		newMsg: state => state.newMsg
	}
})
