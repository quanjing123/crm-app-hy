//huayuan
//quanjing
//quanjing
// export default {
// 	"package": "com.mushichang.huayuancrm",
// 	"baseUrl": "https://api.jincard.cn",
// 	"wssUrl": "wss://socket.jincard.cn/ws",
// 	"uploadUrl": "https://api.jincard.cn",
// 	"ossUrl": "https://jmp-c.oss-cn-shanghai.aliyuncs.com/",
// 	"headImg": "https://qj-c.oss-cn-shanghai.aliyuncs.com/logo/hongmumingpian.png",
// 	"version": "1.0.0",
// 	"versionCode": 100,
// 	"companyKey": "quanjing"
// }


// jxs
export default {
	"package": "com.mushichang.huayuancrm",
	"baseUrl": "https://qjapi.hongmushichang.com",
	"wssUrl": "wss://socket.hongmushichang.com/ws",
	"uploadUrl": "https://qjapi.hongmushichang.com",
	"ossUrl": "https://file.hongmushichang.com/",
	"headImg": "https://file.hongmushichang.com/huayuagoulogo01.pngs",
	"version": "1.5.5",
	"versionCode": 155,
	"companyKey": "quanjing",
	"urlApiPre": "/crmApi" ///crmApi //jxs
}