import {
	clearLoginInfo,
	createBase64
} from './index';

import extJSON from './extJson.js'
let base64 = createBase64();

let {
	companyKey,
	uploadUrl,
	baseUrl,
	ossUrl,
	urlApiPre
} = extJSON;
baseUrl = baseUrl + urlApiPre;
uploadUrl = uploadUrl + '/file/uploadObjectOSS';

wx.setStorageSync("companyKey", companyKey);

var imgBackUrl = ossUrl;

function gettoken() {
	return wx.getStorageSync('token') || ''
}

function getcookie() {
	return wx.getStorageSync('cookies') || ''
}

export function UploadFile(tempFilePaths, request) {
	if (Object.keys(request).length) {
		request = {
			data: base64.encode(JSON.stringify(request)),
		}
	}

	return new Promise((resolve, reject) => {
		uni.uploadFile({
			url: baseUrl + '/template/diy', //仅为示例，非真实的接口地址
			filePath: tempFilePaths[0],
			name: 'file',
			header: {
				'token': gettoken(),
				'cookie': 'PHPSESSID=' + getcookie(),
				'companyKey': companyKey,
				'time': Date.now(),
			},
			formData: request,
			success: (data) => {
				if (data.statusCode == 200) {
					resolve(JSON.parse(data.data));
				} else {
					reject(data.data);
				}
			},
			fail(error) {
				console.error(fail);
			}
		});
	})
}

export function POST(request, method, service, str) {
	console.log("request: " + JSON.stringify(request) + " service:" + service);
	var contentType = 'application/x-www-form-urlencoded',
		url = '';
	if (service == '/home/login/pwd') {
		url = 'https://qjapi.hongmushichang.com/crmApi' + service;
	} else {
		if (method == 'json') {
			contentType = 'multipart/form-data';
			url = baseUrl + service;
		} else {
			url = baseUrl + service;
		}
	}

	let data = request;
	if (Object.keys(request).length) {
		data = {
			data: base64.encode(JSON.stringify(request)),
		}
	}

	return new Promise((resolve, reject) => {
		wx.request({
			url: url,
			data: data,
			method: 'POST',
			header: {
				'content-type': contentType,
				'token': gettoken(),
				'companyKey': companyKey,
				'time': Date.now(),
			},
			success: function(data) {
				if (data.data.code == 200 || data.data.code == 209 || data.data.code == 204) {
					resolve(data.data.data || data.data || '', data.data.code);
				} else if (data.data.code == 401) {
					clearLoginInfo();
					wx.showToast({
						title: '请登录',
						duration: 1000,
						icon: 'none'
					});
					setTimeout(function() {
						uni.clearStorageSync();
						wx.navigateTo({
							url: '/pages/wxLogin/main'
						});
					}, 1000)
				} else if (data.data.code == 403) {
					reject(data.data);
				} else {
					console.log(data)
					reject(data.data);
					let {
						message
					} = data.data;
					message = message ?
						(message === 'GENERAL' ? '网络异常' : message) :
						'网络异常';

					wx.showToast({
						title: message,
						duration: 2000,
						icon: 'none'
					})
				}
			},
			fail: function(err) {
				reject(err);
			}
		})
	})
}

export function GET(request, successed, service) {
	let data = request;
	if (Object.keys(request).length) {
		data = {
			data: base64.encode(JSON.stringify(request)),
		}
	}
	return new Promise((resolve, reject) => {
		wx.request({
			header: {
				'content-type': 'application/json', // 默认值
				'token': gettoken(),
				'companyKey': companyKey,
				'cookie': 'PHPSESSID=' + getcookie(),
			},
			url: baseUrl + service,
			data: data,
			success: function(data) {
				if (data.data.code == 200 || data.data.code == 209) {
					resolve(data.data.data)
				} else if (data.data.code == 401) {
					console.log('登陆123');
					clearLoginInfo();
					wx.showToast({
						title: '请登录',
						duration: 2000,
						icon: 'none'
					});
					setTimeout(function() {
						wx.redirectTo({
							url: '/pages/wxLogin/main'
						});
					}, 1000);
				} else {
					wx.showToast({
						title: data.data.msg || '网络异常',
						duration: 2000,
						icon: 'none'
					})
				}
			},
			fail: function(err) {
				reject(err);
				wx.showToast({
					title: '网络异常',
					duration: 2000,
					icon: 'none'
				})
			}
		})
	})
}

export function DELETE(request, successed, service) {
	return new Promise((resolve, reject) => {
		wx.request({
			header: {
				'content-type': 'application/x-www-form-urlencoded', // 默认值
				'token': gettoken(),
				'companyKey': companyKey,
				'cookie': 'PHPSESSID=' + getcookie(),
			},
			method: 'DELETE',
			url: baseUrl + service,
			data: request,
			success: function(data) {
				if (data.data.code == 200 || data.data.code == 209) {
					resolve(data.data.data)
				} else {
					wx.showToast({
						title: data.data.msg || '网络异常',
						duration: 2000,
						icon: 'none'
					})
				}
			},
			fail: function(err) {
				reject(err);
				wx.showToast({
					title: '网络异常',
					duration: 2000,
					icon: 'none'
				})
			}
		})
	})
}


export function UploadImage(imgPath, method, service) {
	return new Promise((resolve, reject) => {
		wx.uploadFile({
			url: uploadUrl, //
			// https://api.yiliit.com/file/uploadObjectOSS
			//https://192.168.1.130/file/uploadObjectOSS
			filePath: imgPath,
			name: 'file',
			header: {
				'content-type': 'multipart/form-data'
			},
			success(data) {
				if (data.statusCode == 200 || data.data.code == 209) {
					resolve(data.data)
				} else {
					wx.showToast({
						title: data.data.msg || '网络异常',
						duration: 2000,
						icon: 'none'
					})
				}
			},
			fail(err) {
				console.error(err);
				reject(err);
				wx.showToast({
					title: '网络异常',
					duration: 2000,
					icon: 'none'
				})
			}
		})
	})
}


export function checkCollect(request, service) {
	return new Promise((resolve, reject) => {
		wx.request({
			url: baseUrl + '/personal/getIsCollection',
			data: request,
			method: 'POST',
			header: {
				'content-type': 'application/x-www-form-urlencoded',
				'token': gettoken(),
			},
			success: function(data) {
				/*
				*   SUCCESS(200,"成功"),
				    ERROR(500,"系统异常"),
				    FAULT_TOLERANT(501,"容错"),
				    FAIL(201,"操作失败"),
				    NO_LOGIN(401,"未登陆"),
				    NO_ROLE(403,"无权限"),
				    NO_PARAM(202,"参数缺失"),
				    PARAM_CHECK(203,"参数校验失败"),
				    NO_DATA(204,"无数据"),
				    TEST(999999,"TEST")
				* */
				if (data.data.code == 200 || data.data.code == 209) {

					if (data.data.data) {
						if (data.data.data) {
							resolve({
								status: true,
								collectionId: data.data.data.collectionId
							})
						} else {
							resolve({
								status: false
							})
						}
					} else {
						resolve({
							status: false
						});
					}

				} else if (data.data.code == 401) {
					wx.showToast({
						title: '请登录',
						duration: 2000,
						icon: 'none'
					});
					setTimeout(function() {
						var url = '../cardCase/main';
						wx.redirectTo({
							url: url
						});
					}, 1000)
				} else {
					resolve({
						status: false
					});
					// console.log('data--',data);
					/*  wx.showToast({
					      title: data.data.message ||  '网络异常',
					      duration: 2000,
					      icon: 'none'
					  })*/
				}
			},
			fail: function(err) {
				reject(false);
				wx.showModal({
					title: '提示',
					content: JSON.stringify(err),
					success(res) {
						if (res.confirm) {
							console.log('用户点击确定')
						} else if (res.cancel) {
							console.log('用户点击取消')
						}
					}
				})
			}
		})
	})
}

export function changeCollect(request, service) {
	return new Promise((resolve, reject) => {
		wx.request({
			url: baseUrl + service,
			data: request,
			method: 'POST',
			header: {
				'content-type': 'application/x-www-form-urlencoded',
				'token': gettoken(),
			},
			success: function(data) {
				/*
				*   SUCCESS(200,"成功"),
				    ERROR(500,"系统异常"),
				    FAULT_TOLERANT(501,"容错"),
				    FAIL(201,"操作失败"),
				    NO_LOGIN(401,"未登陆"),
				    NO_ROLE(403,"无权限"),
				    NO_PARAM(202,"参数缺失"),
				    PARAM_CHECK(203,"参数校验失败"),
				    NO_DATA(204,"无数据"),
				    TEST(999999,"TEST")
				* */
				if (data.data.code == 200 || data.data.code == 209) {

					resolve(true);

				} else if (data.data.code == 401) {
					wx.showToast({
						title: '请登录',
						duration: 2000,
						icon: 'none'
					});
					setTimeout(function() {
						var url = '../cardCase/main';
						wx.redirectTo({
							url: url
						});
					}, 1500)
				} else {
					resolve(false);
					// console.log('data--',data);
					wx.showToast({
						title: data.data.message || '网络异常',
						duration: 2000,
						icon: 'none'
					})
				}
			},
			fail: function(err) {
				resolve(true);
				wx.showModal({
					title: '提示',
					content: JSON.stringify(err),
					success(res) {
						if (res.confirm) {
							console.log('用户点击确定')
						} else if (res.cancel) {
							console.log('用户点击取消')
						}
					}
				})
			}
		})
	})
}


export function ToPay(request, service) {
	return new Promise((resolve, reject) => {
		wx.request({
			url: baseUrl + '/orders/goPay',
			data: request,
			method: 'POST',
			header: {
				'content-type': 'application/x-www-form-urlencoded',
				'companyKey': companyKey,
				'token': gettoken(),
			},
			success: function(data) {
				/*
				*   SUCCESS(200,"成功"),
				    ERROR(500,"系统异常"),
				    FAULT_TOLERANT(501,"容错"),
				    FAIL(201,"操作失败"),
				    NO_LOGIN(401,"未登陆"),
				    NO_ROLE(403,"无权限"),
				    NO_PARAM(202,"参数缺失"),
				    PARAM_CHECK(203,"参数校验失败"),
				    NO_DATA(204,"无数据"),
				    TEST(999999,"TEST")
				* */
				if (data.data.code == 200 || data.data.code == 209) {

					//data.data.data
					if (data.data.data) {
						let _data = data.data.data;
						wx.requestPayment({
							timeStamp: _data.timeStamp,
							nonceStr: _data.nonce_str,
							package: 'prepay_id=' + _data.prepay_id,
							signType: 'MD5',
							paySign: _data.paySign,
							success(res) {
								console.log(res);
								wx.showToast({
									title: '支付成功',
									icon: 'success'
								});
								resolve(true);
								setTimeout(() => {
									let url = '../orderLists/main';
									wx.navigateTo({
										url: url
									});
								}, 800)
							},
							fail(err) {
								console.log(err);
								reject(false);
								if (err.errMsg === 'requestPayment:fail cancel') {
									wx.showToast({
										title: '支付已取消',
										icon: 'none'
									})
								} else {
									wx.showToast({
										title: '支付失败',
										icon: 'none'
									})
								}

							}
						})
					} else {
						reject(false);
					}


				} else if (data.data.code == 401) {
					reject(false);
					wx.showToast({
						title: '请登录',
						duration: 2000,
						icon: 'none'
					});
					setTimeout(function() {
						var url = '../cardCase/main';
						wx.redirectTo({
							url: url
						});
					}, 1500)

				} else {
					resolve(false);
					// console.log('data--',data);
					wx.showToast({
						title: data.data.message || '网络异常',
						duration: 2000,
						icon: 'none'
					})
				}
			},
			fail: function(err) {
				resolve(false);
				wx.showModal({
					title: '提示',
					content: JSON.stringify(err),
					success(res) {
						if (res.confirm) {
							console.log('用户点击确定')
						} else if (res.cancel) {
							console.log('用户点击取消')
						}
					}
				})
			}
		})
	})
}


export function checkAuth(auths, per) {
	if (auths.indexOf(per) < 0) {
		uni.showModal({
			title: '提示',
			content: '没有访问权限！',
			showCancel: false
		});
		return false;
	}
	return true;
}


export default {
	POST,
	GET,
	DELETE,
	UploadImage,
	imgBackUrl,
	checkCollect,
	changeCollect,
	ToPay,
	UploadFile
}
